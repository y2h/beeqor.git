package models

import "time"

type FilmCategory struct {
	FilmId     int       `gorm:"column:film_id"`
	CategoryId int       `gorm:"column:category_id"`
	LastUpdate time.Time `gorm:"column:last_update;type:timestamp without time zone"`
}
